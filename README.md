# User Management

This serveless service is responsible for manage the users.

### Public Endpoint Visit:
https://3bey9ve9m6.execute-api.us-east-1.amazonaws.com/dev


### Table Schema:
[![finpay.png](https://i.postimg.cc/Dyp5MD3x/finpay.png)](https://postimg.cc/7f7gCK6z)


### Endpoint Information

| Request | Endpoint | Description                       |
|:--------| :------- | :-------------------------------- |
| POST    | `/user`  | Creates a user and client scope record |
| GET     | `/user/:id` | Gets a user and client scope record |
| PATCH   | `/user/:id` | Updates a user and/or client scope record |
| DELETE  | `/user/:id` | Deletes a user and client scope record |

## Quick Start Locally

  The quickest way to get started to start the server as shown below:

Set.env file. For Example:
```console
DB_HOST=localhost
DB_USER=postgres
DB_PASSWORD=admin
DB_NAME=finpay
DB_DIALECT=postgres
UTC_OFFSET=-06:00
```
   
  Install dependencies:

```console
$ npm install
```

  Start the server:

```console
$ npm run start
```

 The default server will run at: http://localhost:3000
 The default endpoints will be at: http://localhost:3000/dev

## Test

### Unit Tests
install the dependencies, then run `npm run test`:

```console
$ npm install
$ npm build
$ npm run test
```

### Integration Tests
first connect to local database
Set.env file. For Example:
```console
DB_HOST=localhost
DB_USER=postgres
DB_PASSWORD=admin
DB_NAME=finpay
DB_DIALECT=postgres
UTC_OFFSET=-06:00
```
then 
 ```console
$ npm install
$ npm run start
$ npm run test
```  



## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)