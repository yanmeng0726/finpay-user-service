import { APIGatewayProxyEvent } from 'aws-lambda';
import { createUser } from '../user/createUserHandler';
import { deleteUserById } from '../user/deleteUserHandler';
import { getUserById } from '../user/getUserHandler';
import { updateUserById } from '../user/updateUserHandler';
import { db, startDB } from '../db';
import { findUserByUserName } from '../services/findUserByUserName';
import { getUserWithAssoc } from '../services/getUserWithAssoc';

let newUserId: string;
describe.skip('get user by id', () => {
  it('should get user by id', async () => {
    const event: APIGatewayProxyEvent = {
      pathParameters: {
        userId: '1'
      },
      headers: {
        origin: 'localhost'
      }
    } as any;

    const result = await getUserById(event);
    expect(result.statusCode).toEqual(200);
    const body = JSON.parse(result.body).response;
    expect(body.firstName).toBe('Apple');
    expect(body.userName).toBe('Apple@gmail.com');
    const clients = body.allowedClients;
    expect(clients.length).toBe(2);
  });
});

describe.skip('create new user', () => {
  it('should create a new user', async () => {
    const event: APIGatewayProxyEvent = {
      body:
        ' {\n' +
        '    "firstName": "Create",\n' +
        '    "lastName": "Handler",\n' +
        '    "mobilePhone": "951-867-8596",\n' +
        '    "userName": "testcreate@gmail.com",\n' +
        '    "allowedClients":[2,3]\n' +
        '   \n' +
        '  }\n',
      headers: {
        origin: 'localhost'
      }
    } as any;

    const result = await createUser(event);
    expect(result.statusCode).toEqual(201);
    const body = JSON.parse(result.body).response;
    newUserId = body.id;
    expect(body.firstName).toBe('Create');
    expect(body.lastName).toBe('Handler');
    expect(body.userName).toBe('testcreate@gmail.com');
    expect(body.allowedClients.length).toBe(2);
    expect(body.userName).toBe('testcreate@gmail.com');
    expect(body.mobilePhone).toBe('951-867-8596');
  });
});

describe.skip('update existing user by id', () => {
  it('should update user', async () => {
    const event: APIGatewayProxyEvent = {
      pathParameters: {
        userId: newUserId
      },
      body: '{\n    "userName": "PineApple@gmail.com",\n    "firstName": "PineApple"\n}',
      headers: {
        origin: 'localhost'
      }
    } as any;

    const result = await updateUserById(event);
    expect(result.statusCode).toEqual(200);
    const body = JSON.parse(result.body).response;
    expect(body.firstName).toBe('PineApple');
    expect(body.userName).toBe('PineApple@gmail.com');
  });
});

describe.skip('delete user by id', () => {
  it('should delete user', async () => {
    let event: APIGatewayProxyEvent = {
      pathParameters: {
        userId: newUserId
      },
      headers: {
        origin: 'localhost'
      }
    } as any;

    let result = await deleteUserById(event);

    expect(result.statusCode).toBe(200);
    event = {
      pathParameters: {
        userId: newUserId
      },
      headers: {
        origin: 'localhost'
      }
    } as any;

    result = await getUserById(event);
    expect(result.statusCode).toEqual(200);
    const body = JSON.parse(result.body).response;
    expect(body).toBe(null);
  });
});

describe.skip('find user by user name', () => {
  it('find user by user name', async () => {
    const DB: db = await startDB();
    const user = await findUserByUserName(DB, 'Apple@gmail.com');
    expect(user).not.toBeNull();
    expect(user['user_name']).toBe('Apple@gmail.com');
  });
});

describe.skip('get user with assoc', () => {
  it('Get user by id with associations', async () => {
    const DB: db = await startDB();
    const user = await getUserWithAssoc(DB, 1);
    expect(user.firstName).toBe('Apple');
    expect(user.userName).toBe('Apple@gmail.com');
    const clients = user.allowedClients;
    expect(clients.length).toBe(2);
  });
});
