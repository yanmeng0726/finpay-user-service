import { convertToSnackCase } from '../../utils/convertToSnackcase';

describe('convert to snack case', () => {
  it('convert all keys to snack case', async () => {
    const obj = convertToSnackCase({ firstName: 'Mandy', lastName: 'Yan' });
    expect(obj['first_name']).toBe('Mandy');
    expect(obj['last_name']).toBe('Yan');
  });
});
