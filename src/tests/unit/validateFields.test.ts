import { validateFields } from '../../utils/validateFields';

describe('validate fields', () => {
  it('throw err if any field is missing', async () => {
    const obj = {};
    const list = ['firstName', 'lastNaame'];
    const errorMsgs = validateFields(obj, list);
    expect(errorMsgs).toBe('firstName is required; lastNaame is required; ');
  });
});
