import {
  internalServerError,
  SuccessResponse,
  BadRequest,
  ConflictRequest
} from '../../utils/response';
describe('check server error returns with correct code', () => {
  it('return internal error', async () => {
    const res = await internalServerError({
      error: { errors: [{ message: 'internal', value: 'internal value' }] }
    });
    expect(res.statusCode).toBe(500);
    const body = JSON.parse(res.body);
    expect(body.message).toBe('Internal server error');
  });

  it('return bad request error', async () => {
    const res = await BadRequest('Bad request');
    expect(res.statusCode).toBe(400);
    const body = JSON.parse(res.body);
    expect(body.message).toBe('Bad request');
  });

  it('return bad request error', async () => {
    const res = await ConflictRequest('Conflict request');
    expect(res.statusCode).toBe(409);
    const body = JSON.parse(res.body);
    expect(body.message).toBe('Conflict request');
  });

  it('return success response', async () => {
    const res = await SuccessResponse('success', 'this is sucess res', 200, 'localhost');
    expect(res.statusCode).toBe(200);
    const body = JSON.parse(res.body);
    expect(body.message).toBe('success');
  });
});
