import { sanitizeData } from '../../utils/sanitizeData';

describe('sanitize data', () => {
  it('change data to camelCase and remove junction table data', async () => {
    const obj = {
      first_name: 'Mandy',
      user_client_scope: [1, 2, 3],
      last_name: 'Yan'
    };
    const updatedObj = sanitizeData(obj);
    expect(updatedObj['firstName']).toBe('Mandy');
    expect(updatedObj['lastName']).toBe('Yan');
  });
});
