import { validateId } from '../../utils/validateId';

describe('validate id', () => {
  it('thorw err if id is not integer', async () => {
    const id = 'aaa';
    let res = validateId(id);
    expect(res).toBe(false);

    res = validateId('1');
    expect(res).toBe(true);
  });
});
