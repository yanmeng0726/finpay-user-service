import { db } from '../db';
import { User } from '../models/user';
import { internalServerError } from '../utils/response';
export async function findUserByUserName(DB: db, userName: string) {
  try {
    let user: Array<User> = await DB.user.findOne({
      where: {
        user_name: userName
      },
      raw: true
    });
    return user;
  } catch (err) {
    throw internalServerError(err);
  }
}
