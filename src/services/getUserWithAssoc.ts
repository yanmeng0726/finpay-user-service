import { db } from '../db';
import { User } from '../models/user';
import { internalServerError } from '../utils/response';
import { sanitizeData } from '../utils/sanitizeData';
export async function getUserWithAssoc(DB: db, id: number) {
  try {
    let user: Array<User> = await DB.user.findOne({
      attributes: { exclude: ['created_dt', 'last_update_dt'] },
      nest: true,
      where: {
        id: id
      },
      include: [{ all: true, model: DB.client, as: 'allowedClients' }]
    });

    let userWithScope: any;
    if (user) {
      const clientsArray = user['dataValues']['allowedClients'].map((x: any) =>
        x.get({ plain: true })
      );

      for (let i = 0; i < clientsArray.length; i++) {
        clientsArray[i] = sanitizeData(clientsArray[i]);
        delete clientsArray[i]['createdAt'];
        delete clientsArray[i]['updatedAt'];
      }
      userWithScope = sanitizeData(user['dataValues']);
      userWithScope['allowedClients'] = clientsArray;
    }

    return userWithScope || user;
  } catch (err) {
    throw internalServerError(err);
  }
}
