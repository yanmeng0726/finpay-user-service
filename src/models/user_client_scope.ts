import { Model, DataTypes, Sequelize } from 'sequelize';
import { db } from '../db';
import { SEED_SCOPE_DATA } from '../seeders/user_client_scope';

export class UserClientScope extends Model {
  public user_client_scope_id?: number;
  public user_id: number;
  public client_id: number;
  public created_dt?: Date;
  public last_update_dt?: Date;
}

export async function initUserClientScope(sequelize: Sequelize) {
  sequelize.define(
    'user_client_scope',
    {
      user_client_scope_id: {
        type: new DataTypes.INTEGER(),
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
      },
      client_id: {
        type: new DataTypes.INTEGER(),
        references: {
          model: sequelize.models.client,
          key: 'id'
        },
        allowNull: false,
        onDelete: 'cascade'
      },
      user_id: {
        type: new DataTypes.INTEGER(),
        references: {
          model: sequelize.models.user,
          key: 'id'
        },
        allowNull: false,
        onDelete: 'cascade'
      },

      created_dt: {
        type: new DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      },
      last_update_dt: {
        type: new DataTypes.DATE(),
        allowNull: true,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      }
    },
    {
      tableName: 'user_client_scope',
      modelName: 'UserClientScope',
      paranoid: true,
      timestamps: false,
      freezeTableName: true,
      underscored: true
    }
  );
}

export async function seedScope(DB: db) {
  const scopes = await DB.user_client_scope.findAndCountAll();

  if (!scopes.count) {
    const data: Array<Pick<UserClientScope, 'user_id' | 'client_id'>> = SEED_SCOPE_DATA;
    return await DB.user_client_scope.bulkCreate(data, { returning: true });
  }
}
