import { Model, DataTypes, Sequelize } from 'sequelize';
import { SEED_CLIENT_DATA } from '../seeders/client';
import { db } from '../db';

export class Client extends Model {
  public id?: number;
  public name: string;
}

export async function initClient(sequelize: Sequelize) {
  sequelize.define(
    'client',
    {
      id: {
        type: new DataTypes.INTEGER(),
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
      },
      name: {
        type: new DataTypes.STRING(256),
        allowNull: false
      }
    },
    {
      tableName: 'client',
      modelName: 'Client',
      timestamps: true,
      freezeTableName: true,
      underscored: true
    }
  );
}

export async function seedClient(DB: db) {
  const clients = await DB.client.findAndCountAll();

  if (!clients.count) {
    const data: Array<Pick<Client, 'name'>> = SEED_CLIENT_DATA;
    return await DB.client.bulkCreate(data, { returning: true });
  }
}
