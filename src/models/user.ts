import { Model, DataTypes, Sequelize } from 'sequelize';
import { SEED_USER_DATA } from '../seeders/user';
import { db } from '../db';
import { Client } from './client';

export class User extends Model {
  public id?: number;
  public last_name: string;
  public first_name: string;
  public mobile_phone: string;
  public created_dt?: Date;
  public last_update_dt?: Date;
  public user_name?: string;
  public allowedClients?: Array<Client>;
}

const phoneValidationRegex = /\d{3}-\d{3}-\d{4}/;

export async function initUser(sequelize: Sequelize) {
  sequelize.define(
    'user',
    {
      id: {
        type: new DataTypes.INTEGER(),
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
      },
      last_name: {
        type: new DataTypes.STRING(256),
        allowNull: false
      },
      first_name: {
        type: new DataTypes.STRING(256),
        allowNull: false
      },
      mobile_phone: {
        type: new DataTypes.STRING(12),
        allowNull: false,
        validate: {
          validator(v: string) {
            if (!phoneValidationRegex.test(v)) {
              throw Error('invalid phone');
            }
            return;
          }
        }
      },
      created_dt: {
        type: new DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      },
      last_update_dt: {
        type: new DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      },
      user_name: {
        type: new DataTypes.STRING(256),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Must be a valid email'
          }
        }
      }
    },
    {
      tableName: 'user',
      modelName: 'User',
      timestamps: false,
      freezeTableName: true,
      underscored: true
    }
  );
}

export async function seedUser(DB: db) {
  const users = await DB.user.findAndCountAll();

  if (!users.count) {
    const data: Array<Pick<User, 'last_name' | 'first_name' | 'mobile_phone' | 'user_name'>> =
      SEED_USER_DATA;
    return await DB.user.bulkCreate(data, { returning: true });
  }
}
