import { UserClientScope } from '../models/user_client_scope';

export const SEED_SCOPE_DATA: Array<Pick<UserClientScope, 'user_id' | 'client_id'>> = [
  {
    user_id: 1,
    client_id: 1
  },
  {
    user_id: 1,
    client_id: 4
  }
];
