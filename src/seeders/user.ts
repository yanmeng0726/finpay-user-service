import { User } from '../models/user';

export const SEED_USER_DATA: Array<
  Pick<User, 'last_name' | 'first_name' | 'mobile_phone' | 'user_name'>
> = [
  {
    last_name: 'Manager',
    first_name: 'Apple',
    mobile_phone: '949-222-3333',
    user_name: 'Apple@gmail.com'
  },
  {
    last_name: 'Manager',
    first_name: 'Banana',
    mobile_phone: '951-222-3333',
    user_name: 'Banana@gmail.com'
  },
  {
    last_name: 'Manager',
    first_name: 'Orange',
    mobile_phone: '951-222-3333',
    user_name: 'Orange@gmail.com'
  }
];
