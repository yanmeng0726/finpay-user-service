import { Client } from '../models/client';

export const SEED_CLIENT_DATA: Array<Pick<Client, 'name'>> = [
  {
    name: 'Mandy Yan'
  },
  {
    name: 'John Smith'
  },
  {
    name: 'Mike Williams'
  },
  {
    name: 'Terry Pratchett'
  }
];
