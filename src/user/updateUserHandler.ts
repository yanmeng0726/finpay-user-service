import { APIGatewayProxyEvent } from 'aws-lambda';
import { db, startDB } from '../db';
import { UserClientScope } from '../models/user_client_scope';
import {
  BadRequest,
  internalServerError,
  sendResponseBody,
  ConflictRequest
} from '../utils/response';
import { convertToSnackCase } from '../utils/convertToSnackcase';
import { validateId } from '../utils/validateId';
import { validateFields } from '../utils/validateFields';
import { findUserByUserName } from '../services/findUserByUserName';
import { getUserWithAssoc } from '../services/getUserWithAssoc';

export const updateUserById = async (event: APIGatewayProxyEvent) => {
  const DB: db = await startDB();
  let t = await DB.sequelize.transaction();
  try {
    if (!event.pathParameters || !event.pathParameters.userId) {
      return BadRequest('userId is required');
    }
    if (!validateId(event.pathParameters.userId)) {
      return BadRequest('userId should be integer');
    }

    const id: number = (event.pathParameters as any).userId;
    if (!event.body) {
      return BadRequest('field for update is required');
    }
    const body = JSON.parse(event.body as string);
    const userFields = convertToSnackCase(body);

    if (userFields['user_name']) {
      const user = await findUserByUserName(DB, userFields['user_name']);
      if (user && user['id'] != id) {
        return ConflictRequest('user name already exists');
      }
    }
    const errMsg = validateFields(userFields, Object.keys(userFields));
    if (errMsg.length) {
      return BadRequest(errMsg);
    }

    if (Object.keys(userFields)) {
      await DB.user.update(
        { ...userFields, last_update_dt: Date.now() },
        { where: { id: id } },
        { transaction: t }
      );
    }

    if (body.allowedClients) {
      if (!body.allowedClients.length) {
        return BadRequest('scope can not be empty');
      }
      let userClientScopes: Array<UserClientScope> = await DB.user_client_scope.findAll(
        {
          raw: true,
          where: {
            user_id: id
          }
        },
        { transaction: t }
      );

      const existingAssociations: Number[] = [];
      for (const scope of userClientScopes) {
        existingAssociations.push(scope.client_id);
      }
      for (const clientId of body.allowedClients) {
        if (!existingAssociations.includes(clientId)) {
          await DB.user_client_scope.create(
            {
              user_id: id,
              client_id: clientId
            },
            { transaction: t }
          );
        }
      }
      for (const currentScope of existingAssociations) {
        if (!body.allowedClients.includes(currentScope)) {
          await DB.user_client_scope.destroy(
            {
              where: {
                user_id: id,
                client_id: currentScope
              }
            },
            { transaction: t }
          );
        }
      }
    }

    await t.commit();
    const message: string = `Successfully updated author with ID: ${id}`;
    const user = await getUserWithAssoc(DB, id);

    return sendResponseBody({
      origin: event.headers.origin,
      resCode: 200,
      success: user,
      message: message
    });
  } catch (error) {
    switch (error.name) {
      case 'SequelizeForeignKeyConstraintError':
        await t.rollback();
        return ConflictRequest('Please make sure allowed client exists');
      case 'SequelizeUniqueConstraintError':
        return ConflictRequest(
          `${error?.errors[0]?.message?.toUpperCase()}. Please check ${error?.errors[0]?.value}`
        );
      case 'SequelizeValidationError':
        return BadRequest(
          `${error?.errors[0]?.message?.toUpperCase()}. Please check ${error?.errors[0]?.value}`
        );
      default:
        return internalServerError(error);
    }
  }
};
