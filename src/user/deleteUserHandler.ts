import { APIGatewayProxyEvent } from 'aws-lambda';
import { middleware } from 'yargs';
import { db, startDB } from '../db';
import { BadRequest, internalServerError, SuccessResponse } from '../utils/response';
import { validateId } from '../utils/validateId';

export const deleteUserById = async (event: APIGatewayProxyEvent) => {
  try {
    if (!event.pathParameters || !event.pathParameters.userId) {
      return BadRequest('userId is required');
    }
    if (!validateId(event.pathParameters.userId)) {
      return BadRequest('userId should be integer');
    }
    const DB: db = await startDB();
    const id: number = (event.pathParameters as any).userId;
    const deletedUser = await DB.user.destroy({
      where: {
        id
      }
    });

    const message: string = deletedUser
      ? `Successfully deleted author with ID: ${id}`
      : `author with ID: ${id} does not exist`;

    return SuccessResponse(message, {}, 200, event.headers.origin);
  } catch (error) {
    return internalServerError(error);
  }
};
