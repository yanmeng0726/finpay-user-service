import { APIGatewayProxyEvent } from 'aws-lambda';
import { db, startDB } from '../db';
import {
  BadRequest,
  ConflictRequest,
  internalServerError,
  SuccessResponse
} from '../utils/response';
import { validateFields } from '../utils/validateFields';
import { getUserWithAssoc } from '../services/getUserWithAssoc';

export const createUser = async (event: APIGatewayProxyEvent) => {
  const DB: db = await startDB();
  let t = await DB.sequelize.transaction();
  try {
    const body = event.body ? JSON.parse(event.body as string) : null;
    const errorMsgs = validateFields(JSON.parse(event.body as string), [
      'firstName',
      'lastName',
      'mobilePhone',
      'userName',
      'allowedClients'
    ]);
    if (errorMsgs.length) {
      return BadRequest(errorMsgs);
    }
    if (!body.allowedClients || !body.allowedClients.length) {
      return BadRequest('scope is required when create new user');
    }

    let newUser = await DB.user.create(
      {
        first_name: body.firstName,
        last_name: body.lastName,
        mobile_phone: body.mobilePhone,
        user_name: body.userName
      },
      { transaction: t }
    );

    const id = newUser['dataValues'].id;
    for (let clientId of body.allowedClients) {
      await DB.user_client_scope.create(
        {
          user_id: id,
          client_id: clientId
        },
        { transaction: t }
      );
    }
    await t.commit();
    newUser = await getUserWithAssoc(DB, newUser['dataValues'].id);
    return SuccessResponse(`Created new user ${id}`, newUser, 201, event.headers.origin);
  } catch (error) {
    switch (error.name) {
      case 'SequelizeForeignKeyConstraintError':
        await t.rollback();
        return ConflictRequest('Please make sure allowed client exists');

      case 'SequelizeUniqueConstraintError':
        return ConflictRequest(
          `${error?.errors[0]?.message?.toUpperCase()}. Please check ${error?.errors[0]?.value}`
        );

      case 'SequelizeValidationError':
        return BadRequest(
          `${error?.errors[0]?.message?.toUpperCase()}. Please check ${error?.errors[0]?.value}`
        );

      default:
        return internalServerError(error);
    }
  }
};
