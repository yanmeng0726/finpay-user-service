import { APIGatewayProxyEvent } from 'aws-lambda';
import { db, startDB } from '../db';
import { validateId } from '../utils/validateId';
import { BadRequest, internalServerError, SuccessResponse } from '../utils/response';
import { getUserWithAssoc } from '../services/getUserWithAssoc';

export const getUserById = async (event: APIGatewayProxyEvent) => {
  try {
    if (!event.pathParameters || !event.pathParameters.userId) {
      return BadRequest('userId is required');
    }

    if (!validateId(event.pathParameters.userId)) {
      return BadRequest('userId should be integer');
    }
    const DB: db = await startDB();
    const id: number = (event.pathParameters as any).userId;
    const user = await getUserWithAssoc(DB, id);
    const message = user ? `Successfully found user with id ${id}` : 'No user found';
    return SuccessResponse(message, user, 200, event.headers.origin);
  } catch (error) {
    return internalServerError(error);
  }
};
