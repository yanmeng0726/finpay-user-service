import { Sequelize } from 'sequelize';
import { User } from '../models/user';
import { Client } from '../models/client';
import { UserClientScope } from '../models/user_client_scope';

export interface dbInterface {
  // Sequelize ORM object
  sequelize: Sequelize;
  // Use this function to establish connection with DB
  connect(): void;
  // Use this function to create associations between tables
  buildRelationships(): void;
  // Use this function to prefill data into tables when empty
  seed(): void;
  // Models for this project
  user: User;
  client: Client;
  user_client_scope: UserClientScope;
}
