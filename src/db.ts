import * as pg from 'pg';
import * as dotenv from 'dotenv';
import { Sequelize } from 'sequelize';
import { dbInterface } from './interfaces/db';
import { initUser, seedUser } from './models/user';
import { initUserClientScope, seedScope } from './models/user_client_scope';
import { initClient, seedClient } from './models/client';

export class db implements dbInterface {
  sequelize: Sequelize;
  user: any;
  client: any;
  user_client_scope: any;

  constructor() {
    dotenv.config({ override: true });
    this.sequelize = new Sequelize(
      process.env.DB_NAME || '',
      process.env.DB_USER || '',
      process.env.DB_PASSWORD,
      {
        host: process.env.DB_HOST,
        dialect: process.env.DB_DIALECT as any,
        dialectModule: pg,
        logging: false
      }
    );

    initUser(this.sequelize);
    initClient(this.sequelize);
    initUserClientScope(this.sequelize);
    this.user = this.sequelize.models.user;
    this.client = this.sequelize.models.client;
    this.user_client_scope = this.sequelize.models.user_client_scope;
  }

  async buildRelationships() {
    this.user.belongsToMany(this.client, { through: this.user_client_scope, as: 'allowedClients' });
    this.client.belongsToMany(this.user, { through: this.user_client_scope });
  }

  async connect() {
    try {
      //Create associations
      await this.buildRelationships();

      //Sync DB
      await this.sequelize
        .sync({ force: false })
        .then(() => console.log('DB Connection established successfully.'))
        .catch((err) => console.error(`DB Sequelize Connection Failed: ${err}`));
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  }

  async seed() {
    await seedClient(this);
    await seedUser(this);
    await seedScope(this);
  }
}

export const startDB = async () => {
  const DB = new db();
  await DB.connect();
  // await DB.seed();
  return DB;
};
