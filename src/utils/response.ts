export interface responseInterface {
  err?: {} | null;
  resCode: number;
  success: {} | null;
  message: string;
  origin?: string;
}

let allowed_origins: any[] = [];

switch (process.env.STAGE) {
  case 'local':
  case 'dev':
    allowed_origins = ['*'];
    break;
  case 'qa':
    allowed_origins = ['*'];
    break;
  case 'prod':
    allowed_origins = ['*'];
    break;
  default:
    allowed_origins = ['*'];
    break;
}

export const sendResponseBody = ({
  err = null,
  resCode,
  success,
  message,
  origin = allowed_origins[0]
}: responseInterface) => {
  const headers = {
    'Access-Control-Allow-Origin': allowed_origins.includes(origin) ? origin : allowed_origins[0],
    'Access-Control-Allow-Credentials': true
  };

  return {
    statusCode: resCode,
    headers,
    body: JSON.stringify(
      {
        message,
        response: err ? err : success
      },
      null,
      2
    )
  };
};

export async function internalServerError(error: any) {
  let message = 'Internal server error';
  if (error?.errors && error?.errors.length) {
    message = `${error?.errors[0]?.message?.toUpperCase()}. Please check ${
      error?.errors[0]?.value
    }`;
  }

  return sendResponseBody({
    err: {
      type: Object.getPrototypeOf(error).constructor.name,
      message: Object.getPrototypeOf(error).constructor.message
    },
    resCode: 500,
    success: null,
    message: message
  });
}

export async function SuccessResponse(message: string, res: any, code: number, origin: any) {
  return sendResponseBody({
    err: null,
    resCode: code ? code : 200,
    success: res,
    message: message,
    origin: origin
  });
}

export async function BadRequest(message: string) {
  return sendResponseBody({
    err: null,
    resCode: 400,
    success: null,
    message: message
  });
}

export async function ConflictRequest(message: string) {
  return sendResponseBody({
    err: null,
    resCode: 409,
    success: null,
    message: message
  });
}
