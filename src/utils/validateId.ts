export function validateId(id: string) {
  return Number.isInteger(Number(id)) && Number(id) > 0;
}
