import * as changeCase from 'change-case';
import * as moment from 'moment';
export function sanitizeData(obj: any) {
  const newObj = {};
  const camelCase = changeCase.camelCase;
  Object.keys(obj).forEach((key) => {
    if (key === 'created_dt' || key == 'last_update_dt') {
      const offset = process.env.UTC_OFFSET ? process.env.UTC_OFFSET : '-06:00';
      obj[key] = obj[key]
        ? moment.utc(obj[key]).utcOffset(offset).format('YYYY-MM-DD HH:mm:ss')
        : null;
      const camelCaseKey = camelCase(key);
      newObj[camelCaseKey] = obj[key];
    } else if (!Array.isArray(obj[key]) && key !== 'user_client_scope') {
      newObj[camelCase(key)] = obj[key];
    }
  });

  return newObj;
}
