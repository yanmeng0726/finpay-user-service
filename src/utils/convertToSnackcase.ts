import * as changeCase from 'change-case';
export function convertToSnackCase(obj: any) {
  const newObj = {};
  const snackCase = changeCase.snakeCase;
  Object.keys(obj).forEach((key) => {
    if (!Array.isArray(obj[key])) {
      newObj[snackCase(key)] = obj[key];
    }
  });

  return newObj;
}
