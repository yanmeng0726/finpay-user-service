export function validateFields(obj: any, list: string[]) {
  let errMessage = '';
  if (!obj || typeof obj !== 'object') {
    for (let i = 0; i < list.length; i++) {
      const key = list[i];
      errMessage += `${key} is required; `;
    }
  } else {
    for (let i = 0; i < list.length; i++) {
      const key = list[i];
      if (!obj[key] || obj[key] == '') {
        errMessage += `${key} is required; `;
      }
    }
  }

  return errMessage;
}
